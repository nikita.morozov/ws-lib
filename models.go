package ws_lib

import (
	"encoding/json"
)

type Message struct {
	Action  string          `json:"action"`
	Topic   string          `json:"topic"`
	Message json.RawMessage `json:"message"`
	Client  *Client
}

type Sender interface {
	Send(topic string, data []byte) error
	SendBroadcast(data []byte) error
}

type WsListener interface {
	Listen(name string, handler HandlerFunc)
}

type HandlerFunc func(data *Message) error
