package ws_lib

import (
	"encoding/json"
	"fmt"
	"gitlab.com/nikita.morozov/ms-shared/tools"
)

type Hub struct {
	receive    chan Message
	register   chan *Client
	unregister chan *Client
	//muSub         sync.Mutex
	clients       map[string]*Client
	Subscriptions []Subscription
	registry      tools.HandlerRegistry
}

func newHub(registry tools.HandlerRegistry) *Hub {
	hub := &Hub{
		receive:       make(chan Message),
		register:      make(chan *Client),
		unregister:    make(chan *Client),
		clients:       make(map[string]*Client),
		Subscriptions: make([]Subscription, 0),
		registry:      registry,
	}

	hub.Listen("subscribe", hub.subscribeHandler)
	hub.Listen("unsubscribe", hub.unsubscribeHandler)
	hub.Listen("ping", hub.pong)

	return hub
}

func (h *Hub) Listen(name string, handler HandlerFunc) {
	h.registry.Add(name, handler)
}

func (h *Hub) Send(topic string, data []byte) error {
	m := Message{
		Topic:   topic,
		Message: data,
	}

	subscriptions := h.GetSubscriptions(topic, nil)
	for _, sub := range subscriptions {
		select {
		case sub.Client.send <- m.Message:
		default:
			unsubMessage := Message{
				Client: sub.Client,
				Topic:  topic,
			}
			return h.unsubscribe(&unsubMessage)
		}
	}

	return nil
}

func (h *Hub) SendBroadcast(data []byte) error {
	m := Message{
		Message: data,
	}

	for clientId := range h.clients {
		select {
		case h.clients[clientId].send <- m.Message:
		default:
			unsubMessage := Message{
				Client: h.clients[clientId],
			}
			return h.unsubscribe(&unsubMessage)
		}
	}

	return nil
}

func (h *Hub) GetSubscriptions(topic string, client *Client) []Subscription {
	var subscriptionList []Subscription

	for _, subscription := range h.Subscriptions {
		if client != nil {
			if subscription.Client.Id == client.Id && subscription.Topic == topic {
				subscriptionList = append(subscriptionList, subscription)
			}
		} else {
			if subscription.Topic == topic {
				subscriptionList = append(subscriptionList, subscription)
			}
		}
	}

	return subscriptionList
}

func (h *Hub) subscribeHandler(data *Message) error {
	h.subscribe(data)
	return nil
}

func (h *Hub) unsubscribeHandler(data *Message) error {
	h.unsubscribe(data)
	return nil
}

func (h *Hub) pong(data *Message) error {
	m := Message{
		Action: "pong",
	}

	payload, err := json.Marshal(m)
	if err != nil {
		return err
	}

	data.Client.send <- payload

	handler := h.registry.Get("onPong")
	if handler != nil {
		err := handler.(HandlerFunc)(data)
		if err != nil {
			fmt.Println(err)
		}
	}

	return nil
}

func (h *Hub) subscribe(data *Message) error {
	fmt.Printf("Subscribe begin :%s\n", data.Client.Id)
	//h.muSub.Lock()

	clientSubs := h.GetSubscriptions(data.Topic, data.Client)

	if len(clientSubs) > 0 {
		return nil
	}

	handler := h.registry.Get("onSubscribe")
	if handler != nil {
		err := handler.(HandlerFunc)(data)
		if err != nil {
			fmt.Println(err)
			return err
		}
	}

	newSubscription := Subscription{
		Topic:  data.Topic,
		Client: data.Client,
	}

	h.Subscriptions = append(h.Subscriptions, newSubscription)
	//h.muSub.Unlock()
	fmt.Printf("Subscribe end :%s\n", data.Client.Id)
	return nil
}

func Filter(vs []Subscription, f func(Subscription) bool) []Subscription {
	filtered := make([]Subscription, 0)
	for _, v := range vs {
		if f(v) {
			filtered = append(filtered, v)
		}
	}
	return filtered
}

func (h *Hub) unsubscribe(data *Message) error {
	fmt.Printf("Unsubscribe begin :%s\n", data.Client.Id)
	data.Client.MakeUnavailable()

	handler := h.registry.Get("onUnsubscribe")
	if handler != nil {
		err := handler.(HandlerFunc)(data)
		if err != nil {
			fmt.Println(err)
			return err
		}
	}

	//h.muSub.Lock()
	var filtered []Subscription
	if len(data.Topic) == 0 {
		filtered = Filter(h.Subscriptions, func(sub Subscription) bool {
			return sub.Client.Id != data.Client.Id && sub.Client.IsAvailable()
		})
	} else {
		filtered = Filter(h.Subscriptions, func(sub Subscription) bool {
			return (sub.Client.Id != data.Client.Id || sub.Topic != data.Topic) && sub.Client.IsAvailable()
		})
	}

	h.Subscriptions = filtered
	//h.muSub.Unlock()

	delete(h.clients, data.Client.Id)
	_, ok := <-data.Client.send
	if ok {
		close(data.Client.send)
	}

	fmt.Printf("Unsubscribe end :%s\n", data.Client.Id)

	return nil
}

func (h *Hub) run() {
	for {
		select {
		case client := <-h.register:
			h.subscribe(&Message{
				Client: client,
			})
		case client := <-h.unregister:
			if _, ok := h.clients[client.Id]; ok {
				h.unsubscribe(&Message{
					Client: client,
				})
			}

		case message := <-h.receive:
			handler := h.registry.Get(message.Action)
			if handler != nil {
				err := handler.(HandlerFunc)(&message)
				if err != nil {
					fmt.Println(err)
				}
			}
		}
	}
}
