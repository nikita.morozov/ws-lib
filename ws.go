package ws_lib

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/nikita.morozov/ms-shared/tools"
)

func WsHandler() (echo.HandlerFunc, WsListener, Sender) {
	registry := tools.NewHandlerRegistry()
	hub := newHub(registry)
	go hub.run()

	return func(context echo.Context) error {
		return handlerWs(hub, context)
	}, hub, hub
}
