package ws_lib

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/nikita.morozov/ms-shared/tools"
	"testing"
	"time"
)

type HubSuite struct {
	suite.Suite
	hub *Hub
}

//TODO: Test for hub filter

func (s *HubSuite) BeforeTest(_, _ string) {
	registry := tools.NewHandlerRegistry()
	s.hub = newHub(registry)
	go s.hub.run()
}

func TestPolicy(t *testing.T) {
	suite.Run(t, new(HubSuite))
}

func (s *HubSuite) Test_subscribe() {
	done := make(chan bool)
	msg1 := Message{
		Action: "subscribe",
		Client: &Client{Id: "1"},
	}
	s.hub.receive <- msg1

	go func() {
		time.Sleep(time.Second)
		done <- true
	}()

	<-done

	msg2 := Message{
		Action: "subscribe",
		Client: &Client{Id: "2"},
	}
	s.hub.receive <- msg2

	go func() {
		time.Sleep(time.Second)
		done <- true
	}()

	<-done

	require.Equal(s.T(), 2, len(s.hub.Subscriptions))

	msg3 := Message{
		Action: "unsubscribe",
		Client: &Client{Id: "1"},
	}
	s.hub.receive <- msg3
	s.hub.receive <- msg3

	msg4 := Message{
		Action: "unsubscribe",
		Client: &Client{Id: "2"},
	}

	s.hub.receive <- msg4
	s.hub.receive <- msg4

	go func() {
		time.Sleep(time.Second)
		done <- true
	}()

	<-done

	require.Equal(s.T(), 0, len(s.hub.Subscriptions))
}

func (s *HubSuite) Test_subscribeFilter() {
	done := make(chan bool)
	msg1 := Message{
		Action: "subscribe",
		Client: &Client{Id: "1"},
	}
	s.hub.receive <- msg1

	go func() {
		time.Sleep(time.Second)
		done <- true
	}()

	<-done

	msg2 := Message{
		Action: "subscribe",
		Client: &Client{Id: "2"},
	}
	s.hub.receive <- msg2

	go func() {
		time.Sleep(time.Second)
		done <- true
	}()

	<-done

	require.Equal(s.T(), 2, len(s.hub.Subscriptions))

	msg3 := Message{
		Action: "unsubscribe",
		Client: &Client{Id: "1"},
	}
	s.hub.receive <- msg3
	s.hub.receive <- msg3

	msg4 := Message{
		Action: "unsubscribe",
		Client: &Client{Id: "2"},
	}

	s.hub.receive <- msg4
	s.hub.receive <- msg4

	go func() {
		time.Sleep(time.Second)
		done <- true
	}()

	<-done

	require.Equal(s.T(), 0, len(s.hub.Subscriptions))
}

func (s *HubSuite) Test_subscribeDiffTopics() {
	done := make(chan bool)
	msg1 := Message{
		Action: "subscribe",
		Topic:  "room",
		Client: &Client{Id: "1"},
	}
	s.hub.receive <- msg1

	go func() {
		time.Sleep(time.Second)
		done <- true
	}()

	<-done

	msg2 := Message{
		Action: "subscribe",
		Topic:  "room/23",
		Client: &Client{Id: "1"},
	}
	s.hub.receive <- msg2

	go func() {
		time.Sleep(time.Second)
		done <- true
	}()

	<-done

	require.Equal(s.T(), 2, len(s.hub.Subscriptions))

	msg3 := Message{
		Action: "unsubscribe",
		Topic:  "room",
		Client: &Client{Id: "1"},
	}
	s.hub.receive <- msg3
	s.hub.receive <- msg3

	msg4 := Message{
		Action: "unsubscribe",
		Topic:  "room",
		Client: &Client{Id: "1"},
	}

	s.hub.receive <- msg4

	go func() {
		time.Sleep(time.Second)
		done <- true
	}()

	<-done

	require.Equal(s.T(), 1, len(s.hub.Subscriptions))
}

func (s *HubSuite) Test_subscribeDiffTopics2() {
	done := make(chan bool)

	go func() {
		msg1 := Message{
			Action: "subscribe",
			Topic:  "room",
			Client: &Client{Id: "1"},
		}
		s.hub.receive <- msg1
	}()

	go func() {
		msg2 := Message{
			Action: "subscribe",
			Topic:  "room/23",
			Client: &Client{Id: "1"},
		}
		s.hub.receive <- msg2
	}()

	go func() {
		time.Sleep(time.Second)
		done <- true
	}()

	<-done

	require.Equal(s.T(), 2, len(s.hub.Subscriptions))

	go func() {
		msg3 := Message{
			Action: "unsubscribe",
			Topic:  "room",
			Client: &Client{Id: "1"},
		}
		s.hub.receive <- msg3
	}()

	go func() {
		msg4 := Message{
			Action: "unsubscribe",
			Topic:  "room/23",
			Client: &Client{Id: "1"},
		}
		s.hub.receive <- msg4
	}()

	go func() {
		time.Sleep(time.Second)
		done <- true
	}()

	<-done

	require.Equal(s.T(), 0, len(s.hub.Subscriptions))
}

func (s *HubSuite) Test_subscribeDiffTopicsStress() {
	done := make(chan bool)
	iters := 100

	for i := 1; i < iters; i++ {
		msg1 := Message{
			Action: "subscribe",
			Topic:  fmt.Sprintf("room%d", i),
			Client: &Client{Id: "1"},
		}
		go func() {
			s.hub.receive <- msg1
		}()
	}

	for i := 1; i < iters; i++ {
		msg3 := Message{
			Action: "unsubscribe",
			Topic:  fmt.Sprintf("room%d", i),
			Client: &Client{Id: "1"},
		}
		go func() {
			s.hub.receive <- msg3
		}()
	}

	go func() {
		time.Sleep(2 * time.Second)
		done <- true
	}()

	<-done

	require.Equal(s.T(), 0, len(s.hub.Subscriptions))
}

func Benchmark_Locked_add(b *testing.B) {
	subscriptions := make([]Subscription, 0)

	iters := 100000
	for i := 1; i < iters; i++ {
		subscriptions = append(subscriptions, Subscription{
			Client: &Client{Id: "1"},
		})
	}

	for iter := 0; iter < b.N; iter++ {
		Filter(subscriptions, func(sub Subscription) bool {
			return sub.Client.Id == "1"
		})
	}
}
